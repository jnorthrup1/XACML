/*
 *                        AT&T - PROPRIETARY
 *          THIS FILE CONTAINS PROPRIETARY INFORMATION OF
 *        AT&T AND IS NOT TO BE DISCLOSED OR USED EXCEPT IN
 *             ACCORDANCE WITH APPLICABLE AGREEMENTS.
 *
 *          Copyright (c) 2013 AT&T Knowledge Ventures
 *              Unpublished and Not for Publication
 *                     All Rights Reserved
 */

/**
 * contains the XACML API for that PDP implementations must meet to be used by the standard PEP
 * implementation.
 *
 * @author car
 * @version $Revision: 1.1 $
 */

package com.att.research.xacml.api.pdp;
