/*
 *                        AT&T - PROPRIETARY
 *          THIS FILE CONTAINS PROPRIETARY INFORMATION OF
 *        AT&T AND IS NOT TO BE DISCLOSED OR USED EXCEPT IN
 *             ACCORDANCE WITH APPLICABLE AGREEMENTS.
 *
 *          Copyright (c) 2013 AT&T Knowledge Ventures
 *              Unpublished and Not for Publication
 *                     All Rights Reserved
 */
package com.att.research.xacml.std.jaxp;

import java.util.Iterator;

import javax.xml.bind.JAXBElement;

import oasis.names.tc.xacml._3_0.core.schema.wd_17.AdviceType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AttributesType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.IdReferenceType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.ObligationType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.ResultType;

import com.att.research.xacml.api.Decision;
import com.att.research.xacml.api.XACML3;
import com.att.research.xacml.std.StdMutableResult;

/**
 * JaxpResult extends {@link com.att.research.xacml.std.StdMutableResult} with methods for creation
 * from JAXP elements.
 * 
 * @author car
 * @version $Revision: 1.1 $
 */
public class JaxpResult extends StdMutableResult {

	protected JaxpResult() {
	}
	
	public static JaxpResult newInstance(ResultType resultType) {
		if (resultType == null) {
			throw new NullPointerException("Null ResultType");
		} else if (resultType.getDecision() == null) {
			throw new IllegalArgumentException("Null Decision in ResultType");
		}
		JaxpResult	jaxpResult	= new JaxpResult();
		
		switch(resultType.getDecision()) {
		case DENY:
			jaxpResult.setDecision(Decision.DENY);
			break;
		case INDETERMINATE:
			jaxpResult.setDecision(Decision.INDETERMINATE);
			break;
		case NOT_APPLICABLE:
			jaxpResult.setDecision(Decision.NOTAPPLICABLE);
			break;
		case PERMIT:
			jaxpResult.setDecision(Decision.PERMIT);
			break;
		default:
			throw new IllegalArgumentException("Invalid Decision in ResultType \"" + resultType.getDecision() + "\"");
		}
		
		if (resultType.getStatus() != null) {
			jaxpResult.setStatus(JaxpStatus.newInstance(resultType.getStatus()));
		}
		
		if (resultType.getObligations() != null && 
			resultType.getObligations().getObligation() != null && 
			resultType.getObligations().getObligation().size() > 0) {
            for (ObligationType obligationType : resultType.getObligations().getObligation()) {
                jaxpResult.addObligation(JaxpObligation.newInstance(obligationType));
            }
		}
		
		if (resultType.getAssociatedAdvice() != null &&
			resultType.getAssociatedAdvice().getAdvice() != null &&
			resultType.getAssociatedAdvice().getAdvice().size() > 0) {
            for (AdviceType adviceType : resultType.getAssociatedAdvice().getAdvice()) {
                jaxpResult.addAdvice(JaxpAdvice.newInstance(adviceType));
            }
		}
		
		if (resultType.getAttributes() != null && resultType.getAttributes().size() > 0) {
            for (AttributesType attributesType : resultType.getAttributes()) {
                jaxpResult.addAttributeCategory(JaxpAttributeCategory.newInstance(attributesType));
            }
		}
		
		if (resultType.getPolicyIdentifierList() != null && 
			resultType.getPolicyIdentifierList().getPolicyIdReferenceOrPolicySetIdReference() != null && 
			resultType.getPolicyIdentifierList().getPolicyIdReferenceOrPolicySetIdReference().size() > 0) {
            for (JAXBElement<IdReferenceType> jaxbElement : resultType.getPolicyIdentifierList().getPolicyIdReferenceOrPolicySetIdReference()) {
                if (jaxbElement.getName().getLocalPart().equals(XACML3.ELEMENT_POLICYIDREFERENCE)) {
                    jaxpResult.addPolicyIdentifier(JaxpIdReference.newInstance(jaxbElement.getValue()));
                } else if (jaxbElement.getName().getLocalPart().equals(XACML3.ELEMENT_POLICYSETIDREFERENCE)) {
                    jaxpResult.addPolicySetIdentifier(JaxpIdReference.newInstance(jaxbElement.getValue()));
                } else {
                    throw new IllegalArgumentException("Unexpected IdReferenceType found \"" + jaxbElement.getName().getLocalPart() + "\"");
                }
            }
		}
		
		return jaxpResult;
	}

}
